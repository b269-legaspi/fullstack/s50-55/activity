import { Button, Row, Col } from "react-bootstrap";

// export default function Banner() {
//   return (
//     <Row>
//       <Col className="p-5">
//         <h1>Zuitt Coding Bootcamp</h1>
//         <p>Opportunities for everyone, everywhere.</p>
//         <Button variant="primary">Enroll now!</Button>
//       </Col>
//     </Row>
//   );
// }

// s53 activity.. undefined route activity.. solution by me

import { Link } from "react-router-dom";

export default function Banner({ bannerProp }) {
  const { title, subtitle, link, buttonmessage } = bannerProp;

  return (
    <Row>
      <Col className="p-5">
        <h1>{title}</h1>
        <p>{subtitle}</p>
        <Button as={Link} to={link} variant="primary">
          {buttonmessage}
        </Button>
      </Col>
    </Row>
  );
}
