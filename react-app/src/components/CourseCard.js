// import {Card, Button} from 'react-bootstrap';

// export default function Banner() {
//   return (
//     <Card className="mb-5 p-3">
//     <Card.Body>
//         <Card.Title>
//             <h5>Sample Course</h5>
//         </Card.Title>
//         <Card.Text>
//             <p>Description:<br></br>
//             This is a sample course offering</p>
//         </Card.Text>
//         <Card.Text>
//             <p>Price:<br></br>
//             Php 40,000</p>
//         </Card.Text>
//         <Button variant="primary">Enroll now!</Button>
//     </Card.Body>
//     </Card>
//     )
//   }
// import { useState } from "react";
import { useState, useEffect } from "react";

// s55 added
import { Link } from "react-router-dom";

// [S50 ACTIVITY]
import { Button, Row, Col, Card } from "react-bootstrap";
// [S50 ACTIVITY END]

// [S50 ACTIVITY]
export default function CourseCard({ course }) {
  // const { name, description, price } = course;
  // s55 changed
  const { name, description, price, _id } = course;

  // commented out s55
  // const [count, setCount] = useState(0);

  // // s51 solution - me
  // // const [slots, setSlots] = useState(30);

  // // function enroll() {
  // //   if (slots > 0){
  // //     setCount(count + 1);
  // //     setSlots(slots -1);
  // //   }
  // //   else {
  // //     alert(`No more seats`)
  // //   }

  // // }

  // // s51 solution - zuitt
  // const [seats, setSeats] = useState(5);

  // function enroll() {
  //   // if (seats > 0) {
  //   //     setCount(count + 1);
  //   //     console.log('Enrollees: ' + count);
  //   //     setSeats(seats - 1);
  //   //     console.log('Seats: ' + seats);
  //   // } else {
  //   //     alert("No more seats available");
  //   // };
  //   setCount(count + 1);
  //   setSeats(seats - 1);
  // }

  // // intro to s52
  // useEffect(() => {
  //   if (seats <= 0) {
  //     alert("No more seats available!");
  //   }
  // }, [seats]);

  return (
    <Row className="mt-3 mb-3">
      <Col xs={12}>
        <Card className="cardHighlight p-0">
          <Card.Body>
            <Card.Title>
              <h4>{name}</h4>
            </Card.Title>
            <Card.Subtitle>Description</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            {/* <Card.Subtitle>Count: {count}</Card.Subtitle> */}
            {/* <Button variant="primary" onClick={enroll}>
              Enroll
            </Button> */}
            {/* changed code for button s55  */}
            <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>
              Details
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
// [S50 ACTIVITY END]
