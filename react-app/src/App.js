// import { useState } from "react";
// added addd s55
import { useState, useEffect } from "react";

import AppNavBar from "./components/AppNavbar";
// added s55 discussion
import CourseView from "./components/CourseView";

import Home from "./pages/Home";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";

// s53 activity.. undefined route activity.. solution by me
import NotFound from "./pages/NotFound";

import { Container } from "react-bootstrap";

// s54 discussion
import { UserProvider } from "./UserContext";

import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import "./App.css";

function App() {
  // <></> fragments -- old code: <fragment></fragment>
  // common pattern in React for component to return multiple elements

  //s54 discussion
  // state hook for the user state that's defined here is for global scope
  // to store the user information and will be used for validating if user is logged in on the app or not
  // same method in appnavbar use state....
  // const [user, setUser] = useState({ email: localStorage.getItem("email") });
  // addd s55 discussion
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  // added s55 discussion
  // Used to check if user information is proprly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        //User is logged in
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        } else {
          setUser({
            id: null,
            isAdmin: null,
          });
        }
      });
  }, []);

  return (
    <>
      {/* userprovider added in s54, will also add globalstate */}
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <AppNavBar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/courses" element={<Courses />} />
              {/* added s55 */}
              <Route path="/courses/:courseId" element={<CourseView />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              {/* // s53 activity.. undefined route activity.. solution by me*/}
              <Route path="*" element={<NotFound />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
