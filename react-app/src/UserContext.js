import React from "react";

// "React.createContext()" us a function in the React Library that creates a new context object
const UserContext = React.createContext();

// "UserContext.Provider" is a component that allows othr components to use the context object and supply th necessary information need to the context object
export const UserProvider = UserContext.Provider; // magkasama lagi tong code na createcontext at provider
// sa documentation, as is na to kaya iccopy paste na lang

export default UserContext;
