import { useState, useEffect } from "react";

// commented out s55, will us mongodb for course data
// import coursesData from "../data/coursesData";
import CourseCard from "../components/CourseCard";

export default function Courses() {
  // s55 discussion
  // to store the courses retrieved from the database
  const [courses, setCourses] = useState([]);

  // console.log(coursesData);
  // const courses = coursesData.map((course) => {
  //   return (
  //     <>
  //       {/* Way to declare props drilling */}
  //       {/* <CourseCard course = {course} />   now working but we need to have a key to access individual course after pressing button */}
  //       <CourseCard key={course.id} course={course} />
  //     </>
  //   );
  // });

  // added in s55

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setCourses(
          data.map((course) => {
            return <CourseCard key={course._id} course={course} />;
          })
        );
      });
  }, []);

  return <>{courses}</>;
}
