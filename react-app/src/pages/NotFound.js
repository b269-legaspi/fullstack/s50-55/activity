// s53 activity.. undefined route activity.. solution by me

import Banner from "../components/Banner";

const data = {
  title: "Error 404 - Page Not Found",
  subtitle: "The page you are looking for cannot be found",
  link: "/",
  buttonmessage: "Back to Home",
};

export default function NotFound() {
  return <Banner bannerProp={data} />;
}
