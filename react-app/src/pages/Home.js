import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

const data = {
  title: "Zuitt Coding Bootcamp",
  subtitle: "Opportunities for everyone, everywhere.",
  link: "/",
  buttonmessage: "Enroll now!",
};

export default function Home() {
  return (
    <>
      <Banner bannerProp={data} />
      <Highlights />
    </>
  );
}
