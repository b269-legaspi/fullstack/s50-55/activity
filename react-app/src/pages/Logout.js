import { Navigate } from "react-router-dom";

// s54 added
import { useContext, useEffect } from "react";
import UserContext from "../UserContext";

export default function Logout() {
  // localStorage.clear();
  const { unsetUser, setUser } = useContext(UserContext);
  unsetUser();

  useEffect(() => {
    // setUser({ email: null });
    //refactored s55
    setUser({ id: null });
  });

  return <Navigate to="/login" />;
}
