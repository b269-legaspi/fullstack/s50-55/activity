import { useState, useEffect, useContext } from "react";

import UserContext from "../UserContext";

import { Navigate, useNavigate } from "react-router-dom";

import { Form, Button } from "react-bootstrap";

import Swal from "sweetalert2";

export default function Register() {
  const { user } = useContext(UserContext);

  // to store values of the input fields
  // const [email, setEmail] = useState("");
  // const [password1, setPassword1] = useState("");
  // const [password2, setPassword2] = useState("");
  // to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  // s55 activity solution by me
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      password1 !== "" &&
      password2 &&
      password1 === password2 &&
      mobileNo !== ""
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, password1, password2, mobileNo]);

  // refactor for s55 ACTIVITY
  // s55 activity solution by me
  function registerUser(e) {
    // Prevents page from reloading
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "Duplicate email found",
            icon: "error",
            text: "Please provide a different email",
          });
        } else {
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
              "Content-type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              password: password1,
              mobileNo: mobileNo,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);

              if (data) {
                Swal.fire({
                  title: "Registration successful",
                  icon: "success",
                  text: "Welcome to Zuitt!",
                });
                setFirstName("");
                setLastName("");
                setEmail("");
                setPassword1("");
                setPassword2("");
                setMobileNo("");
                navigate("/login");
              } else {
                Swal.fire({
                  title: "Something went wrong",
                  icon: "error",
                  text: "Please try again.",
                });
              }
            });
        }
      });
  }

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form onSubmit={(e) => registerUser(e)}>
      <Form.Group controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter first name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter last name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group controlId="mobileNo">
        <Form.Label>Mobile No</Form.Label>
        <Form.Control
          // type="number"
          type="text"
          pattern="[0-9]{11}"
          placeholder="09XXXXXXXXX"
          value={mobileNo}
          onChange={(e) => setMobileNo(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="password2">
        <Form.Label>password2</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
          required
        />
      </Form.Group>

      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}

// refactor for s55 ACTIVITY
// function to simulate user registration
//   function registerUser(e) {
//     // Prevents page from reloading
//     e.preventDefault();

//     // Clear input fields
//     setEmail("");
//     setPassword1("");
//     setPassword2("");

//     alert("Thank you for registering!");
//   }

//   return user.id !== null ? (
//     <Navigate to="/courses" />
//   ) : (
//     <Form onSubmit={(e) => registerUser(e)}>
//       <Form.Group controlId="userEmail">
//         <Form.Label>Email address</Form.Label>
//         <Form.Control
//           type="email"
//           placeholder="Enter email"
//           value={email}
//           onChange={(e) => setEmail(e.target.value)}
//           required
//         />
//         <Form.Text className="text-muted">
//           We'll never share your email with anyone else.
//         </Form.Text>
//       </Form.Group>

//       <Form.Group controlId="password1">
//         <Form.Label>Password</Form.Label>
//         <Form.Control
//           type="password"
//           placeholder="Password"
//           value={password1}
//           onChange={(e) => setPassword1(e.target.value)}
//           required
//         />
//       </Form.Group>

//       <Form.Group controlId="password2">
//         <Form.Label>password2</Form.Label>
//         <Form.Control
//           type="password"
//           placeholder="Verify Password"
//           value={password2}
//           onChange={(e) => setPassword2(e.target.value)}
//           required
//         />
//       </Form.Group>

//       {isActive ? (
//         <Button variant="primary" type="submit" id="submitBtn">
//           Submit
//         </Button>
//       ) : (
//         <Button variant="danger" type="submit" id="submitBtn" disabled>
//           Submit
//         </Button>
//       )}
//     </Form>
//   );
// }

// // import {useState, useEffect} from 'react';

// //s54 activity
// import { useState, useEffect, useContext } from "react";
// import { Navigate } from "react-router-dom";
// import UserContext from "../UserContext";

// import { Form, Button } from "react-bootstrap";

// export default function Register() {
//   // to store and manage value of the input fields
//   const [email, setEmail] = useState("");
//   const [password1, setPassword1] = useState("");
//   const [password2, setPassword2] = useState("");
//   // to determine whether submit button is enabled or not
//   const [isActive, setIsActive] = useState(false);

//   // s54 solution by me
//   const { user } = useContext(UserContext); // useContext(UserContext) is always used..

//   useEffect(() => {
//     if (
//       email !== "" &&
//       password1 !== "" &&
//       password2 !== "" &&
//       password1 === password2
//     ) {
//       setIsActive(true);
//     } else {
//       setIsActive(false);
//     }
//   }, [email, password1, password2]);

//   // function to simulate user registration
//   function registerUser(e) {
//     e.preventDefault();

//     // Clear input fields
//     setEmail("");
//     setPassword1("");
//     setPassword2("");

//     alert("Thank you for registering!");
//   }

//   console.log(user.id); // debugging s55
//   return (
//     // <Form onSubmit={(e) => registerUser(e)} >
//     //     <Form.Group controlId="userEmail">
//     //         <Form.Label>Email address</Form.Label>
//     //         <Form.Control
//     //             type="email"
//     //             placeholder="Enter email"
//     //             value={email}
//     //             onChange={e => setEmail(e.target.value)}
//     //             required
//     //         />
//     //         <Form.Text className="text-muted">
//     //             We'll never share your email with anyone else.
//     //         </Form.Text>
//     //     </Form.Group>

//     //     <Form.Group controlId="password1">
//     //         <Form.Label>Password</Form.Label>
//     //         <Form.Control
//     //             type="password"
//     //             placeholder="Password"
//     //             value={password1}
//     //             onChange={e => setPassword1(e.target.value)}
//     //             required
//     //         />
//     //     </Form.Group>

//     //     <Form.Group controlId="password2">
//     //         <Form.Label>Verify Password</Form.Label>
//     //         <Form.Control
//     //             type="password"
//     //             placeholder="Verify Password"
//     //             value={password2}
//     //             onChange={e => setPassword2(e.target.value)}
//     //             required
//     //         />
//     //     </Form.Group>

//     //     {isActive ?
//     //         <Button variant="primary" type="submit" id="submitBtn">
//     //         Submit
//     //         </Button>
//     //         :
//     //         <Button variant="danger" type="submit" id="submitBtn" disabled>
//     //         Submit
//     //         </Button>
//     //     }

//     // </Form>

//     // refactored for s54 activity
//     // user.email !== null ?
//     // refactored s55 rplaced with user.id

//     user.id !== null ? (
//       <Navigate to="/courses" />
//     ) : (
//       <Form onSubmit={(e) => registerUser(e)}>
//         <Form.Group controlId="userEmail">
//           <Form.Label>Email address</Form.Label>
//           <Form.Control
//             type="email"
//             placeholder="Enter email"
//             value={email}
//             onChange={(e) => setEmail(e.target.value)}
//             required
//           />
//           <Form.Text className="text-muted">
//             We'll never share your email with anyone else.
//           </Form.Text>
//         </Form.Group>

//         <Form.Group controlId="password1">
//           <Form.Label>Password</Form.Label>
//           <Form.Control
//             type="password"
//             placeholder="Password"
//             value={password1}
//             onChange={(e) => setPassword1(e.target.value)}
//             required
//           />
//         </Form.Group>

//         <Form.Group controlId="password2">
//           <Form.Label>Verify Password</Form.Label>
//           <Form.Control
//             type="password"
//             placeholder="Verify Password"
//             value={password2}
//             onChange={(e) => setPassword2(e.target.value)}
//             required
//           />
//         </Form.Group>

//         {isActive ? (
//           <Button variant="primary" type="submit" id="submitBtn">
//             Submit
//           </Button>
//         ) : (
//           <Button variant="danger" type="submit" id="submitBtn" disabled>
//             Submit
//           </Button>
//         )}
//       </Form>
//     )
//   );
// }
